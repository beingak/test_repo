<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/open/admin/');
define('HTTP_CATALOG', 'http://localhost/open/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/open/admin/');
define('HTTPS_CATALOG', 'http://localhost/open/');

// DIR
define('DIR_APPLICATION', '/var/www/html/open/admin/');
define('DIR_SYSTEM', '/var/www/html/open/system/');
define('DIR_LANGUAGE', '/var/www/html/open/admin/language/');
define('DIR_TEMPLATE', '/var/www/html/open/admin/view/template/');
define('DIR_CONFIG', '/var/www/html/open/system/config/');
define('DIR_IMAGE', '/var/www/html/open/image/');
define('DIR_CACHE', '/var/www/html/open/system/cache/');
define('DIR_DOWNLOAD', '/var/www/html/open/system/download/');
define('DIR_UPLOAD', '/var/www/html/open/system/upload/');
define('DIR_LOGS', '/var/www/html/open/system/logs/');
define('DIR_MODIFICATION', '/var/www/html/open/system/modification/');
define('DIR_CATALOG', '/var/www/html/open/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', '127.0.0.1');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'a');
define('DB_DATABASE', 'testing');
define('DB_PREFIX', 'oc_');
